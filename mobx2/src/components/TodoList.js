import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import colors from '../consts/colors';
import Todo from './Todo';
import {inject,observer} from 'mobx-react';

@inject("store")
@observer
class TodoList extends React.Component {
  render(){
    return(
      <View style={styles.todoList}>
        
        {
          this.props.store.todos.map((item,index) => (
            <Todo key ={"todo" + index} index={index} finished ={item.finished} text={item.text}/>
          ))
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  todoList:{
    paddingHorizontal:"5%",
    paddingVertical:25
  },
  headerText:{
      color : colors.inputText,
      //fontSize : 20,
      fontWeight : "bold"
  }
})

export default TodoList;