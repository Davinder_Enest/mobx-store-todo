import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import colors from '../consts/colors'

class Header extends React.Component {
  render(){
    return(
      <View style={styles.header}>
        <Text style={styles.headerText}>Todo List</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  header:{
    width : '100%',
    height : 50,
    backgroundColor : colors.headerBackground,
    justifyContent : 'center',
    paddingHorizontal:"5%"
  },
  headerText:{
      color : colors.headerText,
      fontSize : 20,
      fontWeight : "bold"
  }
})

export default Header;