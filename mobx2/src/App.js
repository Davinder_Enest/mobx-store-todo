import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import Header from './components/Header';
import AddTodo from './components/AddTodo';
import TodoList from './components/TodoList';
import {Provider} from 'mobx-react';
import store from './stores/MainStore';

class App extends React.Component {
  render(){
    return(
      <Provider store={store}>
      <View style={styles.container}>
        <StatusBar/>
        <Header />
        <AddTodo/>
        <TodoList/>
      </View>
      </Provider>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
  }
})

export default App;