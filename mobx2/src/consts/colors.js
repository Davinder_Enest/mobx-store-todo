const colors = {
    headerBackground : '#5F27CD',
    headerText : "#fff",
    statusBarBackground : '#341F97',
    statusBarStyle : 'light-content',
    inputBackground :'#eee',
    buttonBackground : "#10AC84",
    buttonText : '#fff',
    inputText : "#000000",
    todoCircle : '#C4C4C4',
    todoCircleFinished : '#10AC84'
}
export default colors ;