import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import colors from '../consts/colors';
import Icon from 'react-native-vector-icons/Feather';
import {inject,observer} from 'mobx-react';

@inject("store")
@observer
class Todo extends React.Component {
  onPressTodo(){
    this.props.store.toggleTodo(this.props.index);
  }
  render(){
    return(
      <TouchableOpacity 
      onPress = {()=> this.onPressTodo()}
      style={styles.todo}>
        {
            this.props.finished ? (
                <Icon name={"check-circle"} size ={20} color={colors.todoCircleFinished} />
            ) : (
                <Icon name={"circle"} size ={20} color={colors.todoCircle} />
            )
        }
        <Text style={styles.todoText}>{this.props.text}</Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  todo:{
    flexDirection:'row',
    paddingVertical:10,
    alignItems:'center'
  },
  todoText:{
      fontSize:16,
      marginLeft:10
  }
})

export default Todo;